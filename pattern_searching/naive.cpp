// Naive Pattern Searching Algorithm
#include <iostream>
#include <cstring>
using namespace std;

void search(char* pat, char* txt)
{
	int M = strlen(pat);
	int N = strlen(txt);

	/* A loop to slide pattern[] one by one */
	for (int i = 0; i <= N - M; i++) {
		int j;

		/* For current index i, check for pattern match */
		for (j = 0; j < M; j++)
			if (txt[i + j] != pat[j])
				break;

		if (j == M) // if pat[0...M-1] = txt[i, i+1, ...i+M-1]
			cout << "Pattern Matched at Index :  " << i;
				
	}
}
int main()
{
	char txt[] = "atgctcgaatcgatcgaatcgaagtcgcta";
	char pat[] = "tcgc";
	search(pat, txt);
	return 0;
}